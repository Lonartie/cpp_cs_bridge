﻿using System;

// using the wrapper class
using Wrapper;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            // define your input
            IntPtr hwnd = new IntPtr();
            uint crKey = 0;
            byte bAlpha = 0;
            uint dwFlags = 0;

            // call the wrapper function
            bool result = Adapter.SetLayeredWindowAttributes_Bridge(hwnd, crKey, bAlpha, dwFlags);

            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
