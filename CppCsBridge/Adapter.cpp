#include "Adapter.h"

#include <windows.h>

// load the library to link the functions correctly
// the lib is stored in the windows developer kit
#pragma comment(lib,"User32.lib")

using namespace Wrapper;

bool Adapter::SetLayeredWindowAttributes_Bridge(
   System::IntPtr^ hwnd,
   COLORREF crKey,
   BYTE bAlpha, 
   DWORD dwFlags)
{
   // da IntPtr ein managed Typ ist, muss der void* extrahiert werden:
   void* hwnd_cpp = hwnd->ToPointer();
   HWND hwnd_converted = static_cast<HWND>(hwnd_cpp);
   // alles andere liegt bereits im richtigen Format vor, daher kann jetzt direkt die Funktion aufgerufen werden!
   return SetLayeredWindowAttributes(hwnd_converted, crKey, bAlpha, dwFlags);
}
