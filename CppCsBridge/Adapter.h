#pragma once

// COLORREF = DWORD = ULONG = unsigned long
typedef unsigned long ULONG;
typedef ULONG DWORD;
typedef DWORD COLORREF;

// BYTE = UCHAR = unsigned char
typedef unsigned char UCHAR;
typedef UCHAR BYTE;

namespace Wrapper
{
   public ref class Adapter
   {
   public:

      /// @brief COLORREF und BYTE sind Primitivtypen und bedarfen somit keiner expliziten Konvertierung,
      /// @brief um von C# .NET aus aufgerufen werden zu k�nnen (anders sieht es mit IntPtr aus)
      /// @note calls the SetLayeredWindowAttributes windows function with C# parameters
      static bool SetLayeredWindowAttributes_Bridge(
         System::IntPtr^ hwnd,
         COLORREF crKey,
         BYTE bAlpha,
         DWORD dwFlags
      );
   };
}