# CPP-CS-Bridge

## How to compile

* First project (CppCsBridge) is compiled with /clr to enable C# interoperability
* Second project is a normal C# .Net Framework application
* Make sure both projects are compiled with the same architecture (!NEEDED!)
* In the second project, reference CppCsBridge as dependency (as if it was a normal C# project dependency)
* The namespace is required for the C# application to find the managed class of the C++ project
